# firstfit.py
#
# We consider a firstfit allocator for a fragmented region. This has
# bearings on both manual and automatic memory management, as both may
# allocate from fragmented regions; an automatic memory manager does not
# necessarily compact memory.
#
# The simplest allocator is one which just runs along the array looking
# for the first gap that fits. The complexity is bad (O(size of
# memory)), so this is something one would avoid using in practice.

def gap(m,i,empty=0):
    n=len(m)
    j=i
    while j<n:
        if not m[j]==empty: break
        j+=1
    return j-i

def used(m,i,empty=0):
    n=len(m)
    j=i
    while j<n:
        if m[j]==empty: break
        j+=1
    return j-i

def firstfit_simple(gap,used,m,n,i=0):
    while i<len(m):
        g=gap(m,i)
        if n<=g: break
        u=used(m,i+g)
        i+=g+u
    else:
        return None
    return i

#     >>> from firstfit import *
#     >>> import random
#     >>> r=random.Random()
#     >>> r
#     <random.Random object at 0x227f258>
#     >>> r.seed(12345)
#     >>> m=[int(r.random()<0.2) for _ in range(50)]
#     >>> m
#     [0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0]
#     >>> gap(m,0)
#     1
#     >>> used(m,1)
#     1
#     >>> gap(m,2)
#     3
#     >>> used(m,5)
#     1
#     >>> gap(m,6)
#     1
#     >>> used(m,7)
#     2
#     >>> used(m,48)
#     1
#     >>> gap(m,49)
#     1
#     >>> 
#     >>> 
#     >>> len(_)
#     <18>:1: TypeError: object of type 'int' has no len()
#     >>> import functools
#     >>> ff=functools.partial(firstfit_simple,gap,used,m)
#     >>> ff(1)
#     0
#     >>> ff(2)
#     2
#     >>> ff(3)
#     2
#     >>> ff(4)
#     24
#     >>> ff(10)
#     34
#     >>> ff(14)
#     34
#     >>> ff(15)
#     >>> 

# We will look next at the Brent89 allocator. It implements firstfit
# with a couple of auxiliary arrays. It divides memory into segments
# and provides summaries indicating the maximum free size found in the
# segment and also the first link with a segment in a linked list chain
# of sizes threaded through allocated memory blocks.
