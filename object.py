# object.py - python object model for LAMPS
#
# If we have an array of values in python, these can be thought of as
# slots. Let's call the array the memory array. In a high level language
# implementation a sequence of consecutive slots is a typical way to
# represent an object in memory. It is referenced by the location of
# those slots.
#
# In python we can represent the objects we are manipulating in the
# memory array with Python objects in a mostly transparent way. This
# file is for the necessary class and metaclass surgery that is neeeded
# to support these abstract types.
#
# A number together with a type tag can be used to refer to the first
# slot of a number of slots determined by the type.  We can represent
# this number and tag with a subtype of int, in this example Ptr.
#
# We have also to keep track of roots. Python provides a WeakSet in the
# weakref library that will keep a set of weak references to objects. A
# weak reference is one that is only kept if there exist non-weak
# references to the object.
#
# So we make a Cons object out of a Ptr object to use during
# computation. When we construct the Cons object, we remember the
# reference in the WeakSet. When we read from a slot in the Cons object
# we read from the underlying memory array and we make a Cons object for
# any Ptr we find.
#
# When we store a Cons object into a slot, we turn it back into a Ptr.
# This allows the Cons object to be deleted if it is no longer being
# used. If it is deleted, it will disappear from the WeakSet. So the
# WeakSet contains all the Cons objects currently alive in stack frames,
# or that have been stored in structures outside the memory array.
#
# An implementation detail is that the Cons object cannot be a subtype
# of int as these cannot be stored in a weakref.WeakSet.
#
# If we wish to have a moving collector, then we either have to pin i.e.
# not move the instantiated roots, or move all the roots as well as
# memory. The instantiated roots, the roots are exactly the Cons objects
# that have been instantiated from Ptr's.
#
# A further exploration will bring us tags that can be inferred from
# the type and slot position of the referring reference. This opens up
# the posibility of modelling ML types with largely tagless storage.
#
# Arrays of tuples
#
# Python allows us to represent objects with a single location in memory
# if the corresponding cell in the memory array contains a tuple.
#
# A very simple model is fixed size CONS cells in the manner of lisp.
# This simplifies much.
#

import weakref
import graph

class Ptr(int):
    def __repr__(self): return f'{type(self).__name__}({super().__str__()})'

class Cons:
    def __new__(cls,*args,p=None):
        if p is None: p=cls.alloc(len(args))
        obj=super().__new__(cls)
        obj.p=p
        for i,arg in enumerate(args): obj.setslot(i,arg)
        return obj
    def __init_subclass__(cls):
        cls.MEM=[]
        cls.nil=()
        cls.Ptr=Ptr
        super().__init_subclass__()
    @classmethod
    def alloc(cls,n):
        cls.MEM.append([None]*n)
        return len(cls.MEM)-1
    def slot(self,n):
        t=self.MEM[self.p][n]
        return self.__class__(p=t) if isinstance(t, self.Ptr) else t
    def setslot(self, n, x):
        t=self.Ptr(x.p) if isinstance(x, self.__class__) else x
        self.MEM[self.p][n] = t
    def car(self): return self.slot(0)
    def cdr(self): return self.slot(1)
    def setcar(self, x): self.setslot(0, x)
    def setcdr(self, x): self.setslot(1, x)
    def __repr__(self): return f'{type(self).__name__}(p={self.p})'
    def __str__(self): return self.strsexpr(self)

class ConsGC(Cons):
    def __init_subclass__(cls):
        cls.free=[]
        cls.limit=1
        cls.ROOTS=weakref.WeakSet()
        super().__init_subclass__()
    def __new__(cls,*args,p=None):
        obj=super().__new__(cls,*args,p=p)
        cls.ROOTS.add(obj)
        return obj
    @classmethod
    def alloc(cls,n):
        if n!=2: raise ValueError
        if not cls.free and len(cls.MEM)>=cls.limit:
            cls.collect()
        if cls.free: return cls.free.pop()
        cls.MEM.append([None]*n)
        return len(cls.MEM)-1
    @classmethod
    def collect(cls):
        cls.free= list(graph.gc_iterative_mark_sweep([r.p for r in cls.ROOTS],
            {i:[int(x) for x in slots if isinstance(x,cls.Ptr)]
                for i,slots in enumerate(cls.MEM)}))
        cls.free[:]=cls.free[::-1] # reverse free so we pop() from lowest
        if len(cls.free)<len(cls.MEM)/2: cls.limit = (len(cls.MEM)-len(cls.free))*2
        print(f"Collecting... free={len(cls.free)}",end=' ')
        print(f"used={len(cls.MEM)-len(cls.free)} limit={cls.limit}")
        for i in cls.free: cls(cls.nil,cls.nil,p=i) # nil freed memory

from cycles import *

class TestCons1(Strsexpr, Cons): pass
class TestCons2(Strsexpr_refs, ConsGC): pass

#     >>> from object import *
#     >>> cons=TestCons2
#     >>> cons.MEM
#     []
#     >>> cons.ROOTS
#     <_weakrefset.WeakSet object at 0xb64ec610>
#     >>> len(cons.ROOTS)
#     0
#     >>> nil=cons.nil
#     >>> a=cons(2,cons(3,nil))
#     Collecting... free=0 used=1 limit=2
#     >>> list(cons.ROOTS)
#     [TestCons2(p=1)]
#     >>> cons.MEM
#     [[3, ()], [2, Ptr(0)]]
#     >>> 
#     >>> print(list(cons.ROOTS)[0])
#     (2 3)
#     >>> print(a)
#     (2 3)
#     >>> b=cons(1,a)
#     Collecting... free=0 used=2 limit=4
#     >>> b
#     TestCons2(p=2)
#     >>> print(b)
#     (1 2 3)
#     >>> list(cons.ROOTS)
#     [TestCons2(p=1), TestCons2(p=2)]
#     >>> del a
#     >>> list(cons.ROOTS)
#     [TestCons2(p=1), TestCons2(p=2)]
#     >>> b
#     TestCons2(p=2)
#     >>> list(cons.ROOTS)
#     [TestCons2(p=2)]
#     >>> 
#     >>> cons.MEM
#     [[3, ()], [2, Ptr(0)], [1, Ptr(1)]]
#     >>> c=cons('x','y')
#     >>> print(c)
#     (x . y)
#     >>> b.setcdr(c)
#     >>> b
#     TestCons2(p=2)
#     >>> print(b)
#     (1 x . y)
#     >>> cons.MEM
#     [[3, ()], [2, Ptr(0)], [1, Ptr(3)], ['x', 'y']]
#     >>> list(cons.ROOTS)
#     [TestCons2(p=3), TestCons2(p=2)]
#     >>> cons.MEM
#     [[3, ()], [2, Ptr(0)], [1, Ptr(3)], ['x', 'y']]
#     >>> 
#     >>> a=cons('z',b)
#     Collecting... free=2 used=2 limit=4
#     >>> cons.MEM
#     [['z', Ptr(2)], [(), ()], [1, Ptr(3)], ['x', 'y']]
#     >>> 
#     >>> print(a)
#     (z 1 x . y)
#     >>> cons.MEM
#     [['z', Ptr(2)], [(), ()], [1, Ptr(3)], ['x', 'y']]
#     >>> a=cons('w',b)
#     >>> print(a)
#     (w 1 x . y)
#     >>> 
#     >>> cons.MEM
#     [['z', Ptr(2)], ['w', Ptr(2)], [1, Ptr(3)], ['x', 'y']]
#     >>> a.setcdr(a)
#     >>> print(a)
#     #1=(w . #1#)
#     >>> 

# Natural Memory Layout
# 
# A more traditional and perhaps natural way to lay out memory is by
# putting slots in consecutive cells, usually with a header cell to
# denote the number of consecutive slots in an object which will form
# our node, or type from which length may be derived.

class MT:
    """
    Class for object representing empty value.
        >>> from object import v
        >>> type(v)
        <class 'object.MT'>
        >>> v
        v
        >>> print(v)
        v
        >>> v==v
        True
        >>> 
    """
    def __init__(self,name): self.name=name
    def __str__(self): return self.name
    def __repr__(self): return self.name

v=void=MT('v')

from functools import partial
from firstfit import *

class ConsObjGC(Cons):
    def __init_subclass__(cls):
        cls.limit=1
        cls.ROOTS=weakref.WeakSet()
        cls.objs=set()
        super().__init_subclass__()
    def __new__(cls,*args,p=None):
        n=len(args) if args else cls.MEM[p-1]
        if p is None: p=cls.alloc(n)
        obj=object.__new__(cls)
        obj.p=p
        obj.setslot(-1,n)
        for i,arg in enumerate(args): obj.setslot(i,arg)
        cls.ROOTS.add(obj)
        cls.objs.add(p)
        return obj
    def slot(self,k):
        t=self.MEM[self.p+k]
        return self.__class__(p=t) if isinstance(t, self.Ptr) else t
    def setslot(self, k, x):
        t=self.Ptr(x.p) if isinstance(x, self.__class__) else x
        self.MEM[self.p+k] = t
    @classmethod
    def alloc(cls,n):
        ff=partial(firstfit_simple,partial(gap,empty=void),
                partial(used,empty=void),
                cls.MEM)
        N=n+1
        p=ff(N)
        if p is not None: return p+1
        if len(cls.MEM)+N>cls.limit:
            cls.collect()
            p=ff(N)
            if p is not None: return p+1
        cls.MEM+=[v]*N
        return len(cls.MEM)-N+1
    @classmethod
    def edges(cls,i):
        n=cls.MEM[i-1]
        return [j for j in cls.MEM[i:i+n] if type(j)==Ptr]
    @classmethod
    def collect(cls):
        E=graph.FnMapping(cls.edges)
        E.thekeys=cls.objs
        free=graph.gc_iterative_mark_sweep(
            [r.p for r in cls.ROOTS],
            E)
        freespace=0
        for i in free:
            cls.objs.remove(i)
            N=cls.MEM[i-1]+1
            cls.MEM[i-1:i+N-1]=[void]*N # mark freed memory void
            freespace+=N
        total=len(cls.MEM)
        if freespace<total/2: cls.limit = (total-freespace)*2
        print(f"Collecting... free={freespace}",end=' ')
        print(f"used={total-freespace} limit={cls.limit}")

#     >>> from object import *
#     >>> class cons(Strsexpr_refs, Scan_refs,ConsObjGC): pass
#     ... 
#     >>> cons.MEM
#     []
#     >>> cons.ROOTS
#     <_weakrefset.WeakSet object at 0xb6553bd0>
#     >>> len(cons.ROOTS)
#     0
#     >>> nil=cons.nil
#     >>> cons(1,2)
#     Collecting... free=0 used=0 limit=1
#     cons(p=1)
#     >>> print(_)
#     (1 . 2)
#     >>> cons('a',cons('b',cons('c',nil)))
#     Collecting... free=0 used=3 limit=6
#     Collecting... free=0 used=6 limit=12
#     cons(p=10)
#     >>> print(_)
#     (a b c)
#     >>> cons.MEM,cons.objs
#     ([2, 1, 2, 2, 'c', (), 2, 'b', Ptr(4), 2, 'a', Ptr(7)], {1, 10, 4, 7})
#     >>> x=cons('a','b','c')
#     Collecting... free=12 used=0 limit=12
#     >>> cons.MEM
#     [3, 'a', 'b', 'c', v, v, v, v, v, v, v, v]
#     >>> cons('z')
#     cons(p=5)
#     >>> cons.MEM
#     [3, 'a', 'b', 'c', 1, 'z', v, v, v, v, v, v]
#     >>> cons('d')
#     cons(p=7)
#     >>> cons('e','f')
#     cons(p=9)
#     >>> cons.limit
#     12
#     >>> cons('g','h','i','j','k')
#     Collecting... free=7 used=5 limit=12
#     cons(p=5)
#     >>> cons('z')
#     cons(p=11)
#     >>> cons.MEM
#     [3, 'a', 'b', 'c', 5, 'g', 'h', 'i', 'j', 'k', 1, 'z']
#     >>> cons.scan('(#1# b c . #1=(d . #1#))')
#     Collecting... free=8 used=4 limit=12
#     Collecting... free=0 used=12 limit=24
#     cons(p=5)
#     >>> print(_)
#     (#1=(d . #1#) b c . #1#)
#     >>> cons.MEM
#     [3, 'a', 'b', 'c', 2, Ptr(8), Ptr(16), 2, 'd', Ptr(8), v, v, 2, 'c', Ptr(8), 2, 'b', Ptr(13)]
#     >>> 
