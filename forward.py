# forward.py - forwarding of locations
#
# In moving gc, it is common to use iso called "forwarding pointers".
# When an object is moved, a pointer to its new location is left in the
# old location. This is called a forwarding pointer.
#
# But to generalise the concept of forwarding we are simply looking at
# a mapping (in a mathematical sense) from one set of locations to
# another.
#
# We have a location mapping L:G -> L(G), that maps G onto locations.
# And another mapping L':G -> L'(G).  When we compact or otherwise move
# locations from L to L', we can say that the locations L are forwarded
# to L'.
#
# In practice this forwarding might happen at a collection time, or in
# a separate phase.
#
# During a forwarding process, we can have both representations active
# at the same time, and typically that will be the case as we generally
# do not move all objects and update pointer locations at the same
# time. Yet, to the mutator the abstraction must be maintained. So the
# mutator generally sees a consistent set of locations.
#
# The simplest way to maintain the abstraction, is stop-and-collect or
# stop-and-move. We stop the mutator in or to collect and update
# locations until finished, then resume the mutator.
#
# For interleaved or concurrent collection the locations and pointers
# are not all updated at the same time.
