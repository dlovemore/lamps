# compact.py - compaction of numbers.
#
# When thinking about garbage collection, we separate compaction, which
# is removing gaps from our allocated numbers/ranges, from identifying
# unused numbers/ranges.
# 
# In some collectors, such as the classic Cheney collector, there are
# advantages in combining compaction with collection, but this is not
# the general case. Whether we ultimately combine these passes or not,
# we examine them separately to gain a better understanding of these
# procedures.
#
# If we imagine we have allocation and liberation, a way of allocating
# numbers or ranges and way of freeing them up again for reuse. Let's
# look at possible strategies for allocation and reuse.
#
# 1. Allocate sequentially. Never reuse.
# 
# 2. Allocate from freelist. Reuse by appending to list. The first lisp
# systems were like this.
# 
# 3. Allocate from free ranges. This has the issue of identifying free
# ranges. This becomes important when what we allocate are not numbers
# but ranges themselves.
#
# In general, as we cannot control where the freeing happens we
# encounter _fragmentation_.
#
# Fragmentation is what we have when we no longer have nice contiguous
# ranges to allocate from but struggle to find free memory together is
# contiguous regions, that we can use for allocation.
#
# We speak about memory locations. Pointers in representation are
# memory locations but abstractly take on values associated with that
# location. To maintain the abstraction, changes of location have to be
# reflected globally in all objects that point to the location.
#
# Database systems generally maintain the pointer abstraction by never
# changing keys (option 1). It would be theoretically possible to
# change the keys if they were not used as invariant references.
#
# Compaction is the opposite of fragmentation, it is the process of
# closing up gaps between allocated locations. However to do so
# involves renumbering locations that have been allocated.
#
# For this to work we shuffle up the locations that are allocated.
#
# To move a location, we need to identify every place where such a
# location is used and change it to the new location. If we do not do
# this completely precisely something is liable to break.
#
# So when a location is moved the pointers that point to the old
# location needs to be changed to have to the new location. This is
# called forwarding (see forward.py).
#
# For this reason we distinguish pointers which are used to represent
# the abstraction of the value or object being represented and we need
# strict rules governing their use so that the locations of the
# pointers may be moved without the use of the pointers being
# disrupted.
#
# In particular a pointers location cannot be used computationally, they
# are only to be used to access the values. For if the location changes
# the computation needs to change also. That is possible (see locdep.py
# - location dependency) but in general we need to stick to the idea the
# locations are not the pointers, but a hidden internal representation
# of the objects the refer to, and the pointers refer to objects.
#
# Mathemetically, we make a new graph G' that is isomorphic to the old
# graph G. The pointers, are the abstract positions in the graph that
# are referred to via a path from the graph root or roots. The
# locations are distinguished in G, G', but all values and pointers
# from the roots effectively remain the same. We call the map from
# nodes in v in V(G) to v' in V(G'), the compaction map C:V->V'. In
# general this is a very general map and does not even necessarily close
# up gaps. In general case we are going to use the term forwarding map
# but not going to be strict in the distinction.
#
# Pointer example
#
# In javascript we have the object x which is [{1:2}, {a:5, b:6}]
#
# Internally x is represented by the pointer 0x1008 and {1:2} by 0x1048
# and {a:5, b:6} by 0x2060. When an object's location is moved, perhaps
# because of a gc, the locations change but x is still [{1:2},
# {a:5,b:6}]. All the objects that compared equal before compare equal
# now. That is a necessary constraint on memory management, that it
# supports and maintains the abstractions of the language.
#
# Compaction procedure
#
# Compaction is a matter of going through the whole of memory that is
# in use, and replacing v with C(v) for all v. In functional language
# terminology this is G' = map C G.
#
# Efficient compaction
#
# A good place to start with efficient compaction is to look at the
# memory required to store the compaction map. In turn this requires
# minimizing the information stored in such a map. Now, if we have a
# map that closes up the gaps, the least information way to specify
# this involves algorithmically determining the mapping from the
# pattern of gaps. The simplest and most obvious way to do this is to
# move to a location determined by the number of live slots below it
# numerically.
#
# Illustration of compaction
#
#     +----------+               +----------+
# 100 |    A     |-------------->|    A     | m+0
#     +----------+               +----------+
# 101 |  UNUSED  |     .-------->|    P     | m+1
#     +----------+    /          +----------+
# 102 |  UNUSED  |   / .-------->|    Q     | m+2
#     +----------+  / /          +----------+
# 103 |    P     |-- /   .------>|    Z     | m+3
#     +----------+  /   /        +----------+
# 104 |    Q     |--   /
#     +----------+    /
# 105 |  UNUSED  |   /
#     +----------+  /
# 106 |    Z     |--
#     +----------+
# 
# Here m might be 100, or some other location.
# 
# In general what we are calling compaction is any rearrangement of
# memory. What we are seeking however is an efficient way to perform
# compaction by selecting compaction maps that are easy to store and
# compute as well as having any other properties we wish for.
#
import bisect
def compact_simple(gaps,offset=0):
    """
    Return compaction maping thats close up gaps, moving items down.

        >>> from compact import compact_simple
        >>> gaps=(101,102,105)
        >>> C=compact_simple(gaps)
        >>> [(x,C(x)) for x in range(99,108) if x not in gaps]
        [(99, 99), (100, 100), (103, 101), (104, 102), (106, 103), (107, 104)]
        >>> m={100:'A',103:'P',104:'Q',106:'Z'}
        >>> {C(k):v for k,v in m.items()}
        {100: 'A', 101: 'P', 102: 'Q', 103: 'Z'}
        >>> m=2000
        >>> C=compact_simple(gaps,m-100)
        >>> {C(k):v for k,v in {100:'A',103:'P',104:'Q',106:'Z'}.items()}
        {2000: 'A', 2001: 'P', 2002: 'Q', 2003: 'Z'}
        >>> {C(k):v for k,v in reversed(list({100:'A',101:'d',102:'e',103:'P',104:'Q',105:'h',106:'Z'}.items()))}
        {2003: 'Z', 2002: 'Q', 2001: 'P', 2000: 'A'}
    """
    def compact_mapping(x):
        nonlocal gaps,offset
        return x-bisect.bisect(gaps,x)+offset
    return compact_mapping

# An offset allows us to move the sequential locations to any address.
# 
# There's a detail here we should cover while we're here, which is
# the order in which to move locations during a compaction. As the
# compaction closes up gaps, we look for the fixed point of the
# compaction, if there is one and work through the locations starting
# from the fixed point in both directions outward, moving the values
# inward.
#
# We going to say a compaction is the downward compaction if the fixed
# point is at the lowest location being compacted, and a fixed point is
# the upward compaction if the fixed point is at the highest point being
# compacted.
#
def compact_down(gaps): return compact_simple(gaps,0)

def compact_up(gaps): return compact_simple(gaps,len(gaps))

# Notice we are using offsets of 0 and length of the gaps array for the
# downward and upward compaction.
#
# We can now say that using any non-positive offset is a downward
# compaction, and any simple compaction using an offset greater or
# equal to the length of the array is an upward compaction. And for now
# we will call the other cases mixed simple compactions.
#
# To find the fixed point x |-> C(x), we note this is the zero of
# C(x)-x. Addresses with C(x)-x positive are being moved upward, and
# likewise addresses with negative C(x)-x are being move downward.
# Additionally we note that the compaction function is piecewise linear
# with slope one. Equivalently the function C(x)-x is piecewise
# constant, changing only at the gaps. It sufficies therefore to find a
# zero of C(x)-x among the gaps.
#
# We can use a bisection algorithm to find the fixed point. Since the
# python bisect function works on arrays, we will just make the array
# although this can be avoided and isn't notionally part of what we're
# doing here.
#
# Efficiency of Compaction
#
# Any compaction map that closes up the gaps effectively stores the
# shape of the gaps in some way. The attraction of a simple compaction
# where ordering of the nodes is maintained, is that it requires no
# additional information on top of the shape of the gaps.
#
# The efficiency is then driven by a simple compaction being minimal in
# information needed to specify the compaction, which leads to savings
# in memory representation and hopefully also therefore processing
# efficiency.
#
# Practical example
#
# To take a practical example of compaction efficiency. Suppose we have
# a page of 16, 24 or 32 byte objects in memory (so say about 171
# objects). And say that about 1 in 3 is live i.e. about 57 objects. If
# these objects are compacted using forwarding pointers at the head of
# each object, we use 57*8=456 bytes (assuming 8 byte words). But
# because the objects and therefore the forwarding pointers are spread
# out across the page at most 4 and typically 1 or 2 lie in the same
# 64-byte cache line. So we are likely to have to touch more than half
# the page to examine the forwarding pointers for the whole page.
#
# A compact way of remembering the shape of the gaps is either a bitmap
# of words or a list of gap/non-gap boundaries.
#
# A bitmap of 512 bits can be stored in 64 bytes.
#
# A lists of a maximum 57*2=114 edges can be stored as a list of 9-bit
# offsets (or more practically as two lists of 8-bit offsets).
# 114*9=1026 bits for a maximum of just over 128 bytes, but the effiency
# of this representation goes up for larger objects or when live objects
# lie next to each other.
#
# These representations effectively use 20x fewer cache lines to store
# the forwarding maps.

# Compaction with bitset and gap totals.
#
# We can store a list of gaps as a bitset. We can work out the number of
# gaps below a certain point by the number of bits below that point in
# the bitset.
#
# It would be inefficient to store the running total of set bits for
# each position in the bitset, but a compromise where we store a running
# total per word in the bitset is quite reasonable.
#
# These running totals would then take up to as much space again as the
# bitset. This presents us with straightforward compaction map.

from functools import lru_cache
@lru_cache(256)
def bitcount(x): return (x&1)+bitcount(x//2) if x else 0

class BitsetCompact:
    def __init__(self,gaps, offset=0):
        def byte(gs,k): return sum(((k+i) in gs)<<i for i in range(8))
        self.base=min(gaps)
        self.n=max(gaps)-self.base+1
        gs=set(gaps)
        self.bits=[byte(gs,k) for k in
            range(self.base,self.base+self.n,8)]
        self.offset=offset
        c=0
        self.count=[]
        for b in self.bits:
            self.count.append(c)
            c+=bitcount(b)
        self.c=c
    def __call__(self,x):
        def bitsbelow(b,r): return bitcount(b&(1<<r)-1)
        a=x-self.base
        x=x+self.offset
        if a<0: return x
        if not a<self.n: return x-self.c
        p,r=divmod(a,8)
        c=self.count[p]+bitsbelow(self.bits[p],r)
        return x-c

#     >>> from compact import *
#     >>> bitcount(11)
#     3
#     >>> f=BitsetCompact((100,101,105,110,111))
#     >>> f.bits
#     [35, 12]
#     >>> f.count
#     [0, 3]
#     >>> f(99)
#     99
#     >>> f(102)
#     100
#     >>> f(104)
#     102
#     >>> f(105)
#     103
#     >>> f(106)
#     103
#     >>> f(109)
#     106
#     >>> f(112)
#     107
#     >>> [f(x) for x in range(99,113)]
#     [99, 100, 100, 100, 101, 102, 103, 103, 104, 105, 106, 107, 107, 107]
#     >>> 

# Compaction with Edges and Offsets
#
# Another way of representing a compaction map is with edges and offsets
# each range between one edge and the next is offset by an offset
# corresponding to the edge.

