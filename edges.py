# edges.py - block limits sizes remembered
#
# Blocks in memory can be remembered as a list or array of edges.
# Instead of threading memory through the actual blocks we use out of
# memory (OOM) data to record the block placements.
#
# The advantages of this are:
# + The amount of memory used to store information about the blocks can
#   be varied without shuffling the blocks.
# + It's harder for overwrites to corrupt these heap structures.
# + Instead of touching a new cache line when examining each block of
#   memory we can allocate and perform other tasks on more compact
#   structures.
#
# Our basic list of edges is simply going to be a linear list of edges.
# Searching can be binary but inserting edges necessarily shuffles up
# the edges to insert or remove items from the list.

import bisect
from itertools import chain,islice

class Edges:
    def __init__(self):
        self.edges=[]
        self.frees=[]
    def free(self,i):
        a=self.here(i)
        p=self.prev(a)
        if p is not None and self.isfree(p):
            self.popat(a)
            a=p
        q=self.next(a)
        if q is not None and self.isfree(q):
            self.popat(q)
        self.setfree(a,free=1)
    def alloc(self,n):
        b=self.orig()
        j=self.pos(b)
        while j is not None:
            a,i=b,j
            b=self.next(a)
            if b is None: raise ValueError(n)
            j=self.pos(b)
            if self.isfree(a) and j-i>=n: break
        if j-i!=n:
            self.insert(i+n,free=1)
        self.setfree(a,free=0)
        return i
    def orig(self):
        a=0
        return a
    def near(self,i):
        a=bisect.bisect_left(self.edges,i)
        return a
    def here(self,i):
        a=self.near(i)
        assert self.edges[a]==i
        return a
    def prev(self,a):
        return a-1 if a>0 else None
    def next(self,a):
        return a+1 if a<len(self) else None
    def pos(self,a):
        try:
            return self.edges[a]
        except IndexError:
            return None
    def isfree(self,a):
        return self.frees[a]
    def setfree(self,a,free=1):
        self.frees[a]=free
    def insert(self,i,free=0):
        a=self.near(i)
        if self.pos(a)==i:
            self.frees[a]=free
            return
        self.edges.insert(a,i)
        self.frees.insert(a,free)
    def delete(self,i):
        a=self.here(i)
        self.popat(a)
    def popat(self,a):
        self.edges.pop(a)
        self.frees.pop(a)
    def __len__(self): return len(self.edges)
    def __repr__(self):
        return f'{type(self).__name__}({self.edges})'

#     >>> from edges import *
#     >>> bisect.bisect((1,4,10),7)
#     2
#     >>> bisect.bisect((1,4,10),4)
#     2
#     >>> bisect.bisect_left((1,4,10),4)
#     1
#     >>> bisect.bisect((1,4,10),0)
#     0
#     >>> s=Edges()
#     >>> s.insert(3)
#     >>> l=[]
#     >>> l.insert(0,3)
#     >>> l
#     [3]
#     >>> s.insert(4)
#     >>> s.insert(4)
#     >>> s
#     Edges([3, 4])
#     >>> s.insert(0)
#     >>> s
#     Edges([0, 3, 4])
#     >>> 
#     >>> s.delete(3)
#     >>> s
#     Edges([0, 4])
#     >>> s.delete(4)
#     >>> s
#     Edges([0])
#     >>> s.delete(0)
#     >>> s
#     Edges([])
#     >>> s.insert(5)
#     >>> s.delete(4)
#     <24>:1: AssertionError
#     /home/pi/python/lamps/edges.py:80: AssertionError
#       delete(self=Edges([5]), i=4)
#     /home/pi/python/lamps/edges.py:57: AssertionError
#       here(self=Edges([5]), i=4)
#         a=0
#     >>> s=Edges()
#     >>> s.insert(0,1)
#     >>> s.insert(100)
#     >>> s.alloc(4)
#     0
#     >>> s
#     Edges([0, 4, 100])
#     >>> s.alloc(4)
#     4
#     >>> s.alloc(8)
#     8
#     >>> s.alloc(25)
#     16
#     >>> s.free(8)
#     >>> s
#     Edges([0, 4, 8, 16, 41, 100])
#     >>> s.alloc(6)
#     8
#     >>> s.alloc(4)
#     41
#     >>> s.alloc(2)
#     14
#     >>> s.alloc(2)
#     45
#     >>> s.free(16)
#     >>> s.alloc(5)
#     16
#     >>> s.alloc(6)
#     21
#     >>> s.alloc(22)
#     47
#     >>> s.free(47)
#     >>> s
#     Edges([0, 4, 8, 14, 16, 21, 27, 41, 45, 47, 100])
#     >>> 


# An Array or Chunk of Segments
#
# We split the heap into fixed size segments to be able to insert and
# delete edges without shuffling up the whole list.

class SegEdges(Edges):
    def __init__(self,segsize=128):
        self.segs=[]
        self.segsize=segsize
    def orig(self):
        return 0,0
    def near(self,i):
        s,x=divmod(i,self.segsize)
        a=bisect.bisect_left(self.segs[s].edges,x)
        return s,a
    def here(self,i):
        s,a=self.near(i)
        assert self.segs[s].edges[a]==i%self.segsize
        return s,a
    def prev(self,r):
        s,a=r
        if a>0:
            return s,a-1
        while s>0:
            s-=1
            if len(self.segs[s]):
                return s,len(self.segs[s])-1
        return None
    def next(self,r):
        s,a=r
        if a+1<len(self.segs[s]):
            return s,a+1
        while True:
            s+=1
            if s>=len(self.segs): return None
            if len(self.segs[s]):
                return s,0
    def pos(self,r):
        s,a=r
        try:
            return self.segs[s].edges[a]+s*self.segsize
        except IndexError:
            return None
    def isfree(self,r):
        s,a=r
        fs=self.segs[s].frees
        return fs[a] if a<len(fs) else None
    def setfree(self,r,free=1):
        s,a=r
        self.segs[s].frees[a]=free
    def insert(self,i,free=0):
        s,x=divmod(i,self.segsize)
        if s>=len(self.segs):
            self.segs+=[Edges() for _ in range(s+1-len(self.segs))]
        s,a=self.near(i)
        if self.pos((s,a))==x:
            self.setfree((s,a),free)
            return
        seg=self.segs[s]
        seg.edges.insert(a,x)
        seg.frees.insert(a,free)
    def delete(self,i):
        s,a=self.here(i)
        seg=self.segs[s]
        assert a<len(seg.edges)
        assert seg.edges[a]==i
        seg.popat(a)
    def popat(self,r):
        s,a=r
        seg=self.segs[s]
        seg.edges.pop(a)
        seg.frees.pop(a)
    def __repr__(self): return f'SizeSegs({self.segs})'

#     >>> from edges import *
#     >>> c=SegEdges()
#     >>> c.insert(4)
#     >>> c
#     SizeSegs([Edges([4])])
#     >>> c.insert(200)
#     >>> c
#     SizeSegs([Edges([4]), Edges([72])])
#     >>> c.insert(200)
#     >>> c
#     SizeSegs([Edges([4]), Edges([72, 72])])
#     >>> c.delete(5)
#     <54>:1: IndexError: list index out of range
#     /home/pi/python/lamps/edges.py:228: IndexError: list index out of range
#       delete(self=SizeSegs([Edges([4]), Edges([72, 72])]), i=5)
#     /home/pi/python/lamps/edges.py:183: IndexError: list index out of range
#       here(self=SizeSegs([Edges([4]), Edges([72, 72])]), i=5)
#         s=0
#         a=1
#     >>> c.insert(300)
#     >>> c.insert(255,free=1)
#     >>> c
#     SizeSegs([Edges([4]), Edges([72, 72, 127]), Edges([44])])
#     >>> c.alloc(1)
#     255
#     >>> c.alloc(1)
#     256
#     >>> c.alloc(20)
#     257
#     >>> c
#     SizeSegs([Edges([4]), Edges([72, 72, 127]), Edges([0, 1, 21, 44])])
#     >>> 
#     >>> c=SegEdges()
#     >>> c.insert(0,free=1)
#     >>> c.insert(1000)
#     >>> c.alloc(1)
#     0
#     >>> c.alloc(2)
#     1
#     >>> c.alloc(200)
#     3
#     >>> c.alloc(12)
#     203
#     >>> c
#     SizeSegs([Edges([0, 1, 3]), Edges([75, 87]), Edges([]), Edges([]), Edges([]), Edges([]), Edges([]), Edges([104])])
#     >>> c.free(3)
#     >>> c
#     SizeSegs([Edges([0, 1, 3]), Edges([75, 87]), Edges([]), Edges([]), Edges([]), Edges([]), Edges([]), Edges([104])])
#     >>> c.segs[0].frees
#     [0, 0, 1]
#     >>> c.alloc(10)
#     3
#     >>> c
#     SizeSegs([Edges([0, 1, 3, 13]), Edges([75, 87]), Edges([]), Edges([]), Edges([]), Edges([]), Edges([]), Edges([104])])
#     >>> c.free(203)
#     >>> c
#     SizeSegs([Edges([0, 1, 3, 13]), Edges([]), Edges([]), Edges([]), Edges([]), Edges([]), Edges([]), Edges([104])])
#     >>> c.alloc(100)
#     13
#     >>> c.alloc(100)
#     113
#     >>> c.alloc(100)
#     213
#     >>> c
#     SizeSegs([Edges([0, 1, 3, 13, 113]), Edges([85]), Edges([57]), Edges([]), Edges([]), Edges([]), Edges([]), Edges([104])])
#     >>> 
#     >>> c.alloc(300)
#     313
#     >>> c
#     SizeSegs([Edges([0, 1, 3, 13, 113]), Edges([85]), Edges([57]), Edges([]), Edges([101]), Edges([]), Edges([]), Edges([104])])
#     >>> c.free(0)
#     >>> c
#     SizeSegs([Edges([0, 1, 3, 13, 113]), Edges([85]), Edges([57]), Edges([]), Edges([101]), Edges([]), Edges([]), Edges([104])])
#     >>> c.free(1)
#     >>> c
#     SizeSegs([Edges([0, 3, 13, 113]), Edges([85]), Edges([57]), Edges([]), Edges([101]), Edges([]), Edges([]), Edges([104])])
#     >>> c.free(3)
#     >>> c
#     SizeSegs([Edges([0, 13, 113]), Edges([85]), Edges([57]), Edges([]), Edges([101]), Edges([]), Edges([]), Edges([104])])
#     >>> c.free(13)
#     >>> c
#     SizeSegs([Edges([0, 113]), Edges([85]), Edges([57]), Edges([]), Edges([101]), Edges([]), Edges([]), Edges([104])])
#     >>> c.free(113)
#     >>> c
#     SizeSegs([Edges([0]), Edges([85]), Edges([57]), Edges([]), Edges([101]), Edges([]), Edges([]), Edges([104])])
#     >>> c.free(213)
#     >>> c
#     SizeSegs([Edges([0]), Edges([]), Edges([57]), Edges([]), Edges([101]), Edges([]), Edges([]), Edges([104])])
#     >>> c.here(313)
#     (2, 0)
#     >>> c.prev((2,0))
#     (0, 0)
#     >>> 
#     >>> c.free(313)
#     >>> c
#     SizeSegs([Edges([0]), Edges([]), Edges([]), Edges([]), Edges([]), Edges([]), Edges([]), Edges([104])])
#     >>> 
#     >>> 
#     >>> 

# Summary Trees
#
# A Summary tree is a tree with a branching factor of b and n leaves.
#
# To go down one level of the tree we multiply by b and add 1 for the
# first child, 2 for the second child and so on.
#
# Correspondingly to go up one level we subtract 1 and divide by b.
#
# When a leaf value is changed, we propagate the summary upwards until
# the new value we calculate for a node is the same as the old value.

class Summary:
    def __init__(self,f,n,b=2,fill=None):
        self.f=f # summary function
        self.n=n # number of nodes to summarize
        self.b=b # branching factor
        k=1
        while k-1<n: k=self.firstchild(k)
        self.base=k
        self.fill=fill
        self.a=[fill]*(k+n)
    def setf(self,i,*vs):
        self.set(i,self.f(vs))
    def set(self,i,v):
        k=self.base+i
        self.a[k]=v
        while k>0:
            k=self.parent(k)
            v=self.f(self.children(k))
            if v==self.a[k]: break
            self.a[k]=v
    def firstchild(self,k):
        return k*self.b+1
    def children(self,k):
        c=self.firstchild(k)
        return self.a[c:c+self.b]
    def parent(self,k):
        return (k-1)//self.b
    def first(self,n,pos=0):
        if self.f((n,self.a[pos]))!=self.a[pos]: return None
        k=pos
        while k<self.base:
            v=self.a[k]
            if self.f((n,v))==v:
                k=self.firstchild(k)
                while self.f((n,self.a[k]))!=self.a[k]:
                    k+=1
        return k-self.base
    def __len__(self):
        return len(self.a)



#     >>> from edges import *
#     >>> z=Summary(max,9,3,fill=0)
#     >>> z.set(0,9)
#     >>> z.a
#     [9, 9, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0]
#     >>> z.set(1,3)
#     >>> z.a
#     [9, 9, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 9, 3, 0, 0, 0, 0, 0, 0, 0]
#     >>> z.set(3,17)
#     >>> z.a
#     [17, 17, 0, 0, 9, 17, 0, 0, 0, 0, 0, 0, 0, 9, 3, 0, 17, 0, 0, 0, 0, 0]
#     >>> z.set(3,8)
#     >>> z.a
#     [9, 9, 0, 0, 9, 8, 0, 0, 0, 0, 0, 0, 0, 9, 3, 0, 8, 0, 0, 0, 0, 0]
#     >>> z.set(5,11)
#     >>> z.a
#     [11, 11, 0, 0, 9, 11, 0, 0, 0, 0, 0, 0, 0, 9, 3, 0, 8, 0, 11, 0, 0, 0]
#     >>> z.first(9)
#     0
#     >>> z.first(10)
#     5
#     >>> z.set(0,3)
#     >>> z.first(7)
#     3
#     >>> z.first(9)
#     5
#     >>> 

# Summaries with Cursor
#
# There is a potential refinement of this that we do not need to
# propagate values up to the top of the tree, but use a cursor. Values
# not above the cursor are assumed to be correct, and values above the
# cursor are calculated when we move the cursor.
#
# It's tempting, when we are using these summaries for allocation to
# choose a fit from the vicinity of the cursor rather than to ascend to
# the top of the structure. It would give a different behaviour, and
# potentially a beneficial one.

class CursorSummary(Summary):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.c=self.base
        self.r=range(self.base,self.base+1)
    def set(self,i,v):
        while i+self.base not in self.r:
            self.up()
        self.c=i+self.base
        self.r=range(self.c,self.c+1)
        self.a[self.c]=v
    def up(self):
        c=(self.c-1)
        k=c-c//self.b*self.b
        l=len(self.r)
        self.r=range(self.r.start-k*l,self.r.start+(self.b-k)*l)
        self.c=c//self.b
        self.a[self.c]=self.f(self.children(self.c))
    def summarize(self):
        while self.c>0:
            self.up()
    def first(self,n):
        self.summarize()
        return super().first(n)
    def fit(self,n):
        while self.f((n,self.a[self.c]))!=self.a[self.c]:
            if self.c==0: return None
            self.up()
        return super().first(n,pos=self.c)


#     >>> from edges import *
#     >>> z=CursorSummary(max,10,fill=0)
#     >>> z.set(0,3)
#     >>> z.a
#     [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0]
#     >>> z.up()
#     >>> z.a
#     [0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0]
#     >>> 
#     >>> z.set(1,4)
#     >>> z.a
#     [0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0]
#     >>> 
#     >>> z.set(3,9)
#     >>> z.set(4,12)
#     >>> z.set(5,7)
#     >>> z.a
#     [0, 9, 0, 9, 0, 0, 0, 4, 9, 12, 0, 0, 0, 0, 0, 3, 4, 0, 9, 12, 7, 0, 0, 0, 0]
#     >>> z.set(6,13)
#     >>> z.a
#     [0, 9, 0, 9, 12, 0, 0, 4, 9, 12, 0, 0, 0, 0, 0, 3, 4, 0, 9, 12, 7, 13, 0, 0, 0]
#     >>> z.set(7,14)
#     >>> z.a
#     [0, 9, 0, 9, 12, 0, 0, 4, 9, 12, 13, 0, 0, 0, 0, 3, 4, 0, 9, 12, 7, 13, 14, 0, 0]
#     >>> z.first(4)
#     1
#     >>> z.first(13)
#     6
#     >>> z.fit(7)
#     3
#     >>> z.first(7)
#     3
#     >>> z.set(4,5)
#     >>> z.fit(7)
#     5
#     >>> z.a
#     [14, 14, 0, 9, 14, 0, 0, 4, 9, 7, 14, 0, 0, 0, 0, 3, 4, 0, 9, 5, 7, 13, 14, 0, 0]
#     >>> 
#     >>> 

class SizeSegSummary:
    def __init__(self,n,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.summ=Summary(max,n,fill=0)
    def extendto(self,n):
        self.summ.extendto(n)
    def insert(self,i,free=0):
        super().insert(i)
        a=self.here(i)
        b=self.prev(a)
        self.summarizesize(a)
        if b is not None: self.summarizesize(b)
    def delete(self,i):
        a=self.here(i)
        b=self.prev(a)
        self.summarizesize(a)
        self.popat(a)
    def summarizesize(self,a):
        i=self.pos(a)
        s=i//self.segsize
        x=s*self.segsize
        k=j=i
        m=0
        b=a
        while True:
            b=self.prev(b)
            if b is None: break
            j=self.pos(b)
            if j<x: break
            m=max(m,k-j)
            k=j
        b=a
        x+=self.segsize
        k=j=i
        while j<x:
            b=self.next(b)
            if b is None: break
            k=self.pos(b)
            m=max(m,k-j)
            j=k
        if s>=len(self.summ): self.extendto(s+1)
        self.summ.set(s,m)

#     >>> from edges import *
#     >>> class SegTest(SizeSegSummary, SegEdges): pass
#     ... 
#     >>> s=SegTest(10)
#     >>> s.insert(0)
#     >>> s.insert(1000)
#     >>> s.summ.a
#     [1000, 1000, 0, 1000, 0, 0, 0, 1000, 0, 0, 0, 0, 0, 0, 0, 1000, 0, 0, 0, 0, 0, 0, 0, 0, 0]
#     >>> s.insert(200)
#     >>> s.summ.a
#     [800, 800, 0, 800, 0, 0, 0, 800, 0, 0, 0, 0, 0, 0, 0, 200, 800, 0, 0, 0, 0, 0, 0, 0, 0]
#     >>> s.insert(204)
#     >>> s.summ.a
#     [796, 796, 0, 796, 0, 0, 0, 796, 0, 0, 0, 0, 0, 0, 0, 200, 796, 0, 0, 0, 0, 0, 0, 0, 0]
#     >>> s.insert(291)
#     >>> s.summ.a
#     [709, 709, 0, 709, 0, 0, 0, 200, 709, 0, 0, 0, 0, 0, 0, 200, 87, 709, 0, 0, 0, 0, 0, 0, 0]
#     >>> s.insert(999)
#     >>> s.summ.a
#     [708, 708, 0, 708, 1, 0, 0, 200, 708, 0, 1, 0, 0, 0, 0, 200, 87, 708, 0, 0, 0, 0, 1, 0, 0]
#     >>> s.insert(900)
#     >>> s.summ.a
#     [609, 609, 0, 609, 99, 0, 0, 200, 609, 0, 99, 0, 0, 0, 0, 200, 87, 609, 0, 0, 0, 0, 99, 0, 0]
#     >>> s.delete(900)
#     >>> s.summ.a
#     [609, 609, 0, 609, 99, 0, 0, 200, 609, 0, 99, 0, 0, 0, 0, 200, 87, 609, 0, 0, 0, 0, 99, 0, 0]
#     >>> 
