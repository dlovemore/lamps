# cycles.py - approaching gc through input and output of cyclic graphs
#
# One of the major problems of memory management is fragmentation. Some
# forms of garbage collection not only manage what memory is free but
# compact the memory used to represent what's called the heap -- the
# collection of everything that is represented.
#
# If we can print out the values of a structure, cyclic or not, and read
# it back in again, then we have a way of recreating that structure. Do
# this for the roots, and you have a way of reconstructing the entire
# heap. A heap recreated in a natural way will not be fragmented.
#
# Also, as printing cyclic structures involves some form of marking, it
# seems a good thing to provide in a library with other memory
# management routines.
#
# When we print out the heap, we are going to start from the roots.
# There maybe multiple roots that can be represented as some form of
# tuple, so we can generalise either to a single or multiple roots. When
# we output a value, we would like to print this only once. This is
# especially true if we have cycles.
#
# So let us begin by looking at how we print and scan lisp lisp
# expressions; use this to implement a simple gc; and then extend this
# to saving and restoring the heap and portions thereof.
#
# Non-cyclic printing and scanning
#
# Before getting to cyclic printing and scanning let us first examine
# how we print and scan non-cyclic in a simplistic fashion.
#
# These functions are mutually recursive so we are faced with the
# problem that the depth of recursion required to print an expression
# might exceed the stack available. Handling recursion is a general
# problem we face and have to be wary of.

class Symbol(str):
    pass

class Strsexpr:
    @classmethod
    def strsexpr(cls,x):
        if isinstance(x, cls):
            return cls.strcar(x)+cls.strcdr(x)
        else: return str(x)
    @classmethod
    def strcar(cls,x): return '('+cls.strsexpr(x.car())
    @classmethod
    def strcdr(cls,x):
        d=x.cdr()
        if d==cls.nil: return ')'
        if isinstance(d, cls):
            return ' '+cls.strsexpr(d.car())+cls.strcdr(d)
        return ' . '+str(d)+')'

#     >>> from object import *
#     >>> from cycles import *
#     >>> class cons(Strsexpr, ConsGC): pass
#     ... 
#     >>> cons(1,2)
#     cons(p=0)
#     >>> print(_)
#     (1 . 2)
#     >>> cons(1,())
#     Collecting... free=0 used=1 limit=2
#     cons(p=1)
#     >>> print(_)
#     (1)
#     >>> print(cons(cons(3,4),cons(2,())))
#     Collecting... free=1 used=1 limit=2
#     Collecting... free=0 used=2 limit=4
#     ((3 . 4) 2)
#     >>> 

def scansp(s,i):
    while s[i] in ' \t': i+=1
    return i

def scanatom(s,i=0):
    i=scansp(s,i)
    j=i
    while j<len(s) and s[j] not in ' )(': j+=1
    if s[i].isalpha():return Symbol(s[i:j]),j
    return eval(s[i:j]),j

class Scan_simple:
    @classmethod
    def scan(cls,s,i=0):
        r,i=cls.scansexpr(s,i)
        return r
    @classmethod
    def scantail(cls,s,i):
        i=scansp(s,i)
        if s[i]==')': return cls.nil,i+1
        if s[i]=='.':
            x,i = cls.scansexpr(s,i+1)
            i=scansp(s,i)
            if s[i]==')': return x,i+1
            raise ValueError((s,i))
        a,i = cls.scansexpr(s,i)
        d,i = cls.scantail(s,i)
        return cls(a,d),i
    @classmethod
    def scansexpr(cls,s,i=0):
        i=scansp(s,i)
        if s[i]=='(':
            i=scansp(s,i+1)
            if s[i]==')': return cls.nil,i+1
            a,i = cls.scansexpr(s,i)
            d,i = cls.scantail(s,i)
            return cls(a,d),i
        return scanatom(s,i)

#     >>> class cons(Strsexpr, Scan_simple, ConsGC): pass
#     ... 
#     >>> cons.scanatom('23')
#     <11>:1: AttributeError: type object 'cons' has no attribute 'scanatom'
#     >>> cons.scansexpr('(2 . 3)')
#     (cons(p=0), 7)
#     >>> print(_[0])
#     (2 . 3)
#     >>> cons.scansexpr('(2 . 3 x')
#     <14>:1: ValueError: ('(2 . 3 x', 7)
#     /home/pi/python/lamps/cycles.py:110: ValueError: ('(2 . 3 x', 7)
#       scansexpr(cls=<class '__console__.cons'>, s=(2 . 3 x, i=2)
#         a=2
#     /home/pi/python/lamps/cycles.py:99: ValueError: ('(2 . 3 x', 7)
#       scantail(cls=<class '__console__.cons'>, s=(2 . 3 x, i=7)
#         x=3
#     >>> cons.scan('((3 . 4) 2)')
#     Collecting... free=0 used=1 limit=2
#     Collecting... free=0 used=2 limit=4
#     cons(p=3)
#     >>> print(_)
#     ((3 . 4) 2)
#     >>> cons.scan('(a . (b . (c . ())))')
#     Collecting... free=1 used=3 limit=6
#     cons(p=5)
#     >>> print(_)
#     (a b c)
#     >>> x=cons.scan('(a b c)')
#     Collecting... free=3 used=3 limit=6
#     >>> print(x)
#     (a b c)
#     >>> Symbol('a')
#     'a'
#     >>> cons.MEM
#     [['c', ()], ['c', ()], ['b', Ptr(1)], ['a', Ptr(2)], ['b', Ptr(0)], ['a', Ptr(4)]]
#     >>> cons.collect()
#     Collecting... free=3 used=3 limit=6
#     >>> cons.MEM
#     [[(), ()], ['c', ()], ['b', Ptr(1)], ['a', Ptr(2)], [(), ()], [(), ()]]
#     >>> 

# Printing and scanning with cycles
#
# LISP has a syntax for printing cycles, which is something like
#
# #1=value. Define reference
# #1# refer to defined reference.
# So (#1=(1 2) #1#) is ((1 2) (1 2)) where the (1 2) are shared.
# As I understand it, forward references are not allowed, but clearly
# the syntax is extensible to allow forward references.
#
# We will restrict ourselves to sharing only CONS cells.
#
# Now we imagine a stateful printer, that as it prints cons cells it
# remembers them. If it has already seen a cell it prints the reference
# for that cell instead of the cell. But how do we know in advance which
# cells need references. We print twice, in the second printing, we know
# to print the definition (def) the first time the cell is seen and just
# the reference (ref) on subsequent times.

class Refs:
    class Ref(int):
        hwm=0
        def __new__(cls,i=None):
            if i is None: cls.hwm+=1; i=cls.hwm
            return super().__new__(cls,i)
        # def __str__(self): return self.__repr__()
        def __repr__(self):
            return type(self).__name__+'('+super().__str__()+')'
    seen=set()
    multiple=set()
    refs={}
    again=False
    @classmethod
    def reset(cls):
        cls.seen.clear()
        cls.multiple.clear()
        cls.refs.clear()
        cls.Ref.hwm=0
        cls._again=False
    @classmethod
    def found(cls,x):
        first=x.p not in cls.seen
        if first: cls.seen.add(x.p)
        elif x.p not in cls.multiple:
            cls.multiple.add(x.p)
            cls._again=True
        if x.p in cls.multiple:
            if x.p not in cls.refs: cls.refs[x.p]=cls.Ref()
            return '#'+str(cls.refs[x.p])+('=' if first else '#')
        return ''
    @classmethod
    def again(cls):
        if not cls._again: return False
        cls._again=False
        cls.seen.clear()
        cls.refs.clear()
        cls.Ref.hwm=0
        return True

class Strsexpr_refs:
    @classmethod
    def strsexpr(cls,x):
        Refs.reset()
        while True:
            s=cls.strsexpr1(x)
            if Refs.again(): continue
            return s
    @classmethod
    def strsexpr1(cls,x):
        if isinstance(x, cls):
            s = Refs.found(x)
            if s[-1:]=='#': return s
            return s+cls.strcar(x)+cls.strcdr(x)
        return str(x)
    @classmethod
    def strcar(cls,x): return '('+cls.strsexpr1(x.car())
    @classmethod
    def strcdr(cls,x):
        d=x.cdr()
        if d==cls.nil: return ')'
        if isinstance(d, cls):
            s = Refs.found(d)
            if s[-1:]=='#': return ' . '+s+')'
            t=cls.strsexpr1(d.car())+cls.strcdr(d)
            if s[-1:]=='=': return ' . '+s+'('+t+')'
            return ' '+t
        return ' . '+str(d)+')'

#     >>> from cycles import *
#     >>> class cons(Strsexpr_refs, ConsGC): pass
#     ... 
#     >>> nil=cons.nil
#     >>> cons(1,2)
#     cons(p=0)
#     >>> print(_)
#     (1 . 2)
#     >>> cons(1,())
#     Collecting... free=0 used=1 limit=2
#     cons(p=1)
#     >>> print(_)
#     (1)
#     >>> print(cons(cons(4,5),cons(6,cons.nil)))
#     Collecting... free=1 used=1 limit=2
#     Collecting... free=0 used=2 limit=4
#     ((4 . 5) 6)
#     >>> Refs.seen
#     {Ptr(0), Ptr(2), 3}
#     >>> Refs.multiple
#     set()
#     >>> a=cons(1,2)
#     Collecting... free=4 used=0 limit=4
#     >>> a.setcar(a)
#     >>> print(a)
#     #1=(#1# . 2)
#     >>> a.setcdr(a)
#     >>> print(a)
#     #1=(#1# . #1#)
#     >>> b=cons(a,cons(2,cons(a,nil)))
#     >>> print(b)
#     (#1=(#1# . #1#) 2 #1#)
#     >>> c=cons(cons(a,b),cons(b,cons(a,nil)))
#     Collecting... free=0 used=4 limit=8
#     >>> print(c)
#     ((#1=(#1# . #1#) . #2=(#1# 2 #1#)) #2# #1#)
#     >>> 

# The reason we print with state rather than passing an extra parameter
# is so that this interacts better with Python structures, which might
# come in useful.

# Scanning with cycles, no forward references
#
# When we scan, without forward references, we simply need to recognise
# reference definitions and uses (defs and refs), and either remember
# them or use the reference already defined.
#
# However, when we define a reference, we need to remember an associated
# cons cell. In the way we have previously done our scanning, the cons
# cell is not allocated until we have finished scanning the whole
# substructure. This needs to change.
#
# nfr stands for no forward references

def scanref(s,i):
    if s[i]=='#': i=i+1
    else: raise ValueError((s,i))
    j=i
    while s[j] not in '#=': j+=1
    ri=eval(s[i:j])
    return Refs.Ref(ri),j+1,s[j]

class Scan_nfr:
    @classmethod
    def scan(cls,s,i=0):
        Refs.reset()
        r,i=cls.scansexpr(s,i)
        return r
    @classmethod
    def scantail(cls,s,i):
        i=scansp(s,i)
        if s[i]==')': return cls.nil,i+1
        if s[i]=='.':
            x,i = cls.scansexpr(s,i+1)
            i=scansp(s,i)
            if s[i]==')': return x,i+1
            raise ValueError((s,i))
        a,i = cls.scansexpr(s,i)
        d,i = cls.scantail(s,i)
        return cls(a,d),i
    @classmethod
    def scansexpr(cls,s,i=0):
        c=()
        i=scansp(s,i)
        if s[i]=='#':
            r,i,rd = scanref(s,i)
            i=scansp(s,i)
            if rd=='=':
                c=cls((),())
                Refs.refs[r]=c
            elif rd=='#':
                return Refs.refs[r],i
        if s[i]=='(':
            if not c: c=cls((),())
            i=scansp(s,i+1)
            if s[i]==')': return nil,i+1
            a,i = cls.scansexpr(s,i)
            d,i = cls.scantail(s,i)
            c.setcar(a)
            c.setcdr(d)
            return c,i
        return scanatom(s,i)

#     >>> from cycles import *
#     >>> class cons(Strsexpr_refs, Scan_nfr, ConsGC): pass
#     ... 
#     >>> cons.scan('(a b)')
#     Collecting... free=0 used=1 limit=2
#     cons(p=0)
#     >>> print(_)
#     (a b)
#     >>> cons.scan('#1=(a b . #1#)')
#     Collecting... free=0 used=2 limit=4
#     cons(p=2)
#     >>> print(_)
#     #1=(a b . #1#)
#     >>> cons.scan('((#1=(#1# . #1#) . #2=(#1# b #1#)) #2# #1#)')
#     Collecting... free=2 used=2 limit=4
#     Collecting... free=0 used=4 limit=8
#     Collecting... free=0 used=8 limit=16
#     cons(p=0)
#     >>> print(_)
#     ((#1=(#1# . #1#) . #2=(#1# b #1#)) #2# #1#)
#     >>> 

# Scanning with forward references
#
# Now if we wish to cope with forward references we can leave
# placeholders at the sites of the references we have yet to resolve.
# These placeholders then need to be filled in after the definition has
# been scanned.
#
# Taking this idea a little further a reference can be realised as an
# actual object in the structure being scanned. The resolution of these
# references is then a graph transformation akin to linking. We hope
# a proper grasp of these concepts helps us understand how to save and
# restore from the heap.
#
# In particular if we describe all our references with Ref objects that
# look like #1#, #2#, etc. and combine that with definitions that look
# like #1=... then linking these references joins the whole structure
# together.
#
# This opens us the possibility of breaking down the heap into smaller
# structures, or even individual objects and linking them all via
# references.

class Scan_refs:
    @classmethod
    def scantail(cls,s,i):
        i=scansp(s,i)
        if s[i]==')': return cls.nil,i+1
        if s[i]=='.':
            x,i = cls.scansexpr(s,i+1)
            i=scansp(s,i)
            if s[i]==')': return x,i+1
            raise ValueError((s,i))
        a,i = cls.scansexpr(s,i)
        d,i = cls.scantail(s,i)
        return cls(a,d),i
    @classmethod
    def scansexpr(cls,s,i=0):
        c=()
        i=scansp(s,i)
        if s[i]=='#':
            r,i,rd = scanref(s,i)
            i=scansp(s,i)
            if rd=='=':
                c=cls((),())
                Refs.refs[r]=c
            elif rd=='#':
                # if Refs.refs[r]: return Refs.refs[r],i
                return r,i
        if s[i]=='(':
            if not c: c=cls((),())
            i=scansp(s,i+1)
            if s[i]==')': return nil,i+1
            a,i = cls.scansexpr(s,i)
            d,i = cls.scantail(s,i)
            c.setcar(a)
            c.setcdr(d)
            return c,i
        return scanatom(s,i)
    @classmethod
    def scan(cls,s,i=0):
        Refs.reset()
        r,i=cls.scansexpr(s,i)
        return cls.resolve_refs(r)
    @classmethod
    def resolve_refs(cls,r,seen=None):
        if seen is None: seen=set()
        if isinstance(r, Refs.Ref):
            return Refs.refs[r]
        if isinstance(r,cls):
            if r in seen: return r
            r.setcar(cls.resolve_refs(r.car(),seen))
            r.setcdr(cls.resolve_refs(r.cdr(),seen))
        return r

#     >>> from cycles import *
#     >>> class cons(Strsexpr_refs, Scan_refs, ConsGC): pass
#     ... 
#     >>> cons.scan('#1=(a . #1#)')
#     cons(p=0)
#     >>> print(_)
#     #1=(a . #1#)
#     >>> _.cdr()
#     cons(p=0)
#     >>> Refs.reset()
#     >>> cons.scansexpr('#1=(a . #1#)')
#     Collecting... free=0 used=1 limit=2
#     (cons(p=1), 12)
#     >>> cons.resolve_refs(_[0])
#     cons(p=1)
#     >>> print(_)
#     #1=(a . #1#)
#     >>> _.cdr()
#     cons(p=1)
#     >>> cons.scan('#1=(a . #1#)')
#     Collecting... free=1 used=1 limit=2
#     cons(p=0)
#     >>> print(_)
#     #1=(a . #1#)
#     >>> x=(cons.scan('((#1=(#1# . #1#) . #2=(#1# b #1#)) #2# #1#)'))
#     Collecting... free=1 used=1 limit=2
#     Collecting... free=0 used=2 limit=4
#     Collecting... free=0 used=4 limit=8
#     Collecting... free=0 used=8 limit=16
#     >>> print(x)
#     ((#1=(#1# . #1#) . #2=(#1# b #1#)) #2# #1#)
#     >>> 
