# graph.py
#
# Garbage Collection can be seen as retrieving nodes disconnected from
# a graph.
#
# In line with how these graphs are formed and used in a computer
# program, we will look at the case of directed edges in a directed
# graph.
#
# If x is a node, let E(x) is the set edges of that node, that is the
# edges x -> y. Programmatically these can be stored simply as the
# edges y.
#
# In a real program the edges are going to be numbered in some way.
# (Each slot has a different edge.) But this can be largely ignored
# from the point of view of GC.
#
# To represent this programmatically we can have a function or a
# dictionary, E where E[x]=list of y s.t. x->y is an edge.

# Recursive mark sweep on graph:
#
# As you can see, a basic mark sweep algorithm is ten lines. What we
# are theoretically doing is not complicated. However getting the right
# mix of data structures and algorithms to do it efficiently adds
# richness and complexity to the problem.
# 
def gc_recursive_mark_sweep(R,E):
    """
    Garbage collect the graph (E.keys(),E) from the roots R.
    return set of keys of E that can be reclaimed.

    This involves identifying all nodes in V not connected from the
    roots R.

        >>> from graph import *
        >>> E={1: {2,3,4}, 2: {8,9}, 3: {5,6}, 4: {},
        ...    5: {1,2}, 6: {}, 7: {7}, 8:{1,2}, 9:{}, 10: {1}}
        >>> sorted(gc_recursive_mark_sweep({1},E))
        [7, 10]
        >>> 
    """
    marked = set()
    def mark(x):
        if x not in marked:
            marked.add(x)
            for v in E[x]:
                mark(v)
    for v in R:
        mark(v)
    return set(E.keys()).difference(marked)

# Iterative mark sweep
#
# The first issue we will face with recursive algorithms is they can
# blow stack limits in languages that don't support unlimited
# recursion. This algorithm therefore presents itself.
#
def gc_iterative_mark_sweep(R,E):
    """
    Garbage collect the graph (E.keys(),E) from the roots R.
    return set of keys of E that can be reclaimed.

    This involves identifying all nodes in V not connected from the
    roots R.

        >>> from graph import *
        >>> E={1: {2,3,4}, 2: {8,9}, 3: {5,6}, 4: {},
        ...    5: {1,2}, 6: {}, 7: {7}, 8:{1,2}, 9:{}, 10: {1}}
        >>> sorted(gc_iterative_mark_sweep({1},E))
        [7, 10]
        >>> 
    """
    grey=set(R)
    marked=set()
    while grey:
        for x in list(grey):
            grey.remove(x)
            marked.add(x)
            for y in E[x]:
                if y not in marked:
                    grey.add(y)
    return set(E.keys()).difference(marked)

# Virtual Edge Mapping
#
# In the examples shown the edge map E is a dictionary containing the
# list of all edges from every graph node. Python allows us
# to use a virtual mapping, allowing us to discover edges dynamically
# from a calculated function instead.
#
class FnMapping:
    def __init__(self,fn):
        self.fn=fn
        self.thekeys={}
    def __getitem__(self, x):
        return self.fn(x)
    def keys(self): return self.thekeys
