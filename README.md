LAMPS - A Lightweight Alternative to the Memory Pool System
===========================================================

The MPS (Memory Pool System) is sophisticated memory manager originally
in library form that can be used to perform a number of memory
managment tasks. In particular it can be used to perform garbage
collection for new or existing computer languages.

LAMPS is intended as a lightweight alternative to the MPS, which will
be easier to modify and experiment with, and provide an expository
framework for memory management concepts.

Purpose
-------

A memory manager is a computer program that allocates and frees
portions of computer memory for use by another computer program or
language system for storing information in normally consecutive memory
locations. (For this reason the job can be seen as close to managing
allocation of numbers or ranges of numbers.)

A garbage collector is a computer program or procedure that finds
unused memory and recycles it by freeing it for reallocation. It may
also rearrange existing memory typically to compact the representation
of allocated memory and free up contiguous blocks. Unusable memory is
detected by finding memory that is no longer connected via a sequences
of edges, represented by pointers from the roots. (Strictly speaking it
is usually disconnected memory rather than simply usued memory that
gets recycled.)

The terms memory manager and garbage collector are sometimes used
interchangeably but memory manager is slightly broader as it includes
management that supports manual freeing.

A computer language or other computer system that needs to represent
objects in memory will often use garbge collection to recycle unused
memory.

The MPS is designed for changing memory mangement needs, but much of
the design took place over 20 years ago, and it can be difficult to
easily change a mature programming system.

The idea of LAMPS, is not simply to do the job in a slimmed down and
lightweight fashion, but to act as an alternative environment for
experimentation and inspiration that may feed back into the design of
other memory managers and also hopefully the MPS itself.

LAMPS is intended to help those writing their own memory managers
wrestle with and comprehend the concepts behind memory management. Some
of the exposition will be quite mathematical in nature, but it is the
aim to make these concepts comprehensible.

Plan
----

1. Produce a documented memory manager in Python as a working
prototype.

2. On the way, pick up a use case, which is space efficient managed
memory in Python.

3. Partially migrate the memory manager to C by generating C code as
needed from Python code. The generation is not intended to be an
automatic translation, but code written in Python to generate code in a
different language.
